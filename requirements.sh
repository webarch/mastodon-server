#!/usr/bin/env bash

set -e -o pipefail

if [[ "${1}" ]]; then
  echo "Running: ansible-galaxy install -r requirements.yml --force ${1}"
  ansible-galaxy install -r requirements.yml --force "${1}"
else
  echo "Running: ansible-galaxy install -r requirements.yml --force -vv"
  ansible-galaxy install -r requirements.yml --force
fi

